<?php
function validateCard ( $cardnumber ){
	$cardnumber = preg_replace ( " /\D|\s/ " , "" , $cardnumber ) ;  # strip any non-digits
	$cardlength = strlen ( $cardnumber ) ;
	if ( $cardlength != 0 )
	{
		$parity = $cardlength % 2 ;
		$sum = 0 ;
		for ( $i = 0 ; $i < $cardlength ; $i ++ )
		{
			$digit = $cardnumber [ $i ] ;
			if ( $i % 2 == $parity ) $digit = $digit * 2 ;
			if ( $digit > 9 ) $digit = $digit - 9 ;
			$sum = $sum + $digit ;
		}
		$valid = ( $sum % 10 == 0 ) ;
		return $valid ;
	}
	return false ;
}

function getRootPath(){
	return str_replace('App/Common/common.php','', str_replace('\\', '/', __FILE__));
}