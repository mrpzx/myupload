<?php
import('ORG.Util.Session');
class IndexAction extends Action {
    public function index(){
    	$step = 0;
    	if(!($step = intval(Session::get('step')))){
    		Session::set('step', '1');
    		$step = 1;
    	}
    	$uid = 0;
    	$User = D('Users');
    	//显示上一步上传的照片
    	if($step > 1 && $step < 7){
    		$file_info_pre = $User->getFileInfo($step - 1);
    		$File = D('Files');
    		if(!($uid = intval(Session::get('uid')))){
    			Session::clear();
    			$this->error('上传已超时，请重新上传');
    		}
    		
    		$thumb = $File->where("`users_id`=$uid and `key`='".$file_info_pre['key']."'")->getField('thumb');
    		$this->assign('thumb', $thumb);
    	}
    	$this->assign('step', $step);
    	if($step < 7){
	    	$file_info = $User->getFileInfo($step);
	    	$this->assign('file_des', $file_info['des']);
	    	$this->assign('file_key', $file_info['key']);
	    	$this->assign('uid', $uid);
    	} else{
    		Session::clear();
    	}
		$this->display();
    }
    
    public function upload() {
    	if(!($step = intval(Session::get('step')))){
    		Session::clear();
    		$this->error("上传已超时，请重新上传");
    	}
    	if($step == 1){
	    	if(!isset($_POST['name']) || empty($_POST['name'])){
				$this->error('请输入姓名');
			}
			if(!isset($_POST['phone']) || empty($_POST['phone'])){
				$this->error('请输入手机号码');
			}
			if(C('IS_WANLIAN') == 1){
	    		if(!isset($_POST['creditno']) || empty($_POST['creditno'])){
					$this->error('请输入常用信用卡号');
				}
	    		if(!isset($_POST['bankname']) || empty($_POST['bankname'])){
					$this->error('请输入银行名称');
	    		}
			}
    	} else {
    		if(!isset($_POST['uid']) || empty($_POST['uid'])){
    			$this->error('上传失败，请稍后重试');
    		}
    		$uid = intval($_POST['uid']);
    	}
   	
		$savepath = 'Uploads/';
		$now = explode('/', date('Y/m/d'));
		foreach($now as $v){
			$nextpath = $savepath . $v . '/';
			if(!file_exists($nextpath)){
				if(!mkdir($nextpath)){
					break;
				} 
			}
			$savepath = $nextpath;
		}
		
    	import('ORG.Net.UploadFile');
    	import('ORG.Util.Image');
    	$upload = new UploadFile();// 实例化上传类
    	$upload->maxSize  = 10485760 ;// 设置附件上传大小
    	$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
    	$upload->savePath =  $savepath;// 设置附件上传目录
		$upload->thumb = true; //生成缩略图
		$upload->thumbMaxWidth = '50,200';
		$upload->thumbMaxHeight = '50,200';
		$upload->thumbRemoveOrigin = false;
    	if(!$upload->upload()) {// 上传错误提示错误信息
    		$this->error($upload->getErrorMsg());
    	}else{// 上传成功
    		$info = $upload->getUploadFileInfo();
    	}
		
    	$root_path = C('ROOT_PATH');
    	$f = array(
    		'users_id'=> $uid,
			'origin'  => $root_path.'/'.$info[0]['savepath'].$info[0]['savename'],
			'thumb'   => $root_path.'/'.$info[0]['savepath'].'thumb_'.$info[0]['savename'],
			'ext'	  => $info[0]['extension'],
    		'key'	  => $info[0]['key'],
    		'hash'	  => $info[0]['hash']
    	);
    	
    	if($step == 1){
			$data = array();
			$data['name'] = $_POST['name'];
			$data['phone'] = $_POST['phone'];
			if(C('IS_WANLIAN') == 1){
				$data['creditno'] = $_POST['creditno'];
				$data['bankname'] = $_POST['bankname'];
			}
			$data['ischeck'] = 0;

			$uid = D("Users")->data($data)->add(); // 写入用户数据到数据库
			$f['users_id'] = $uid;
			Session::set('uid', $uid);
    	} 
    	
    	D('Files')->data($f)->add();
		
    	Session::set('step', ++$step);
		$this->success('上传成功！');
    }
    
    public function cancel(){
    	if(!($step = intval(Session::get('step')))){
    		Session::clear();
    		$this->error("上传已超时，请重新上传", U("Index/index"));
    	}
    	if($step < 2 || $step > 6){
    		$this->error("步骤不正确，只有上传第一张图片后未完成上传前可以取消");
    	}
    	if(!($uid = intval(Session::get('uid')))){
    		Session::clear();
    		$this->error('上传已超时，请重新上传', U("Index/index"));
    	}
    	//步骤正确，用户信息正确，删除已上传的文件，删除当前用户记录
    	$User = D('Users'); 
        if(!($result = $User->relation(true)->where("id=$uid")->find())){
        	Session::clear();
            $this->error("无法删除当前用户，请重新上传", U("Index/index"));
        }
        
         //删除文件
        $root_path = getRootPath();
        foreach($result['Files'] as $row){
            if(file_exists($root_path . $row['origin'])){
                unlink($root_path . $row['origin']);
            }
            if(file_exists($root_path . $row['thumb'])){
                unlink($root_path . $row['thumb']);
            }
        }
        //删除记录
        $User->relation(true)->delete("$uid");
        Session::clear();
        $this->success("取消成功，你可以重新上传或放弃",U("Index/index"));
    }
}