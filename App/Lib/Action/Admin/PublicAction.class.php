<?php
  class PublicAction extends Action{
      public function checkLogin(){
           if(!isset($_POST['username']) || !isset($_POST['password']))  {
               $this->error('请输入用户名');
           }
           $username = $_POST['username'];
           $password = $_POST['password'];
           $password = md5(md5($password));
           $Admin = M('admin');
           if(!($userinfo = $Admin->where("`username`='$username' and `password`='$password'")->find())){
                $this->error('用户名或密码错误');
           }
           $Admin->where("id=".$userinfo['id'])->setField('last_login_time',time());     //登陆成功，更新最后登录时间
           $_SESSION[C('ADMIN_AUTH_KEY')] = $userinfo['id'];
           unset($userinfo['password']);
           
           import('ORG.Util.Session') ;
           Session::set('admin', $userinfo);
           Session::setExpire(C('SESSION_EXPIRE_TIME'), true);
           //Cookie::set('admin_id', $userinfo['id']);
           $this->success('登陆成功', U('Index/index'));
      }                                         
      
      public function login(){
          if(!isset($_SESSION[C('AMIN_AUTH_KEY')])){
              $this->display();
          } else {
              redirect(U('Index/index'));
          }
      }
      
      public function loginOut(){
          if(isset($_SESSION[C('ADMIN_AUTH_KEY')])) {
             unset($_SESSION[C('ADMIN_AUTH_KEY')]);
             unset($_SESSION['admin']);
             $this->success('你已成功退出', U('Public/login'));
          } else{
              $this->error('注销成功', U('Public/login'));
          }
      }
      
      public function index(){
          redirect(U('Index/index'));
      }
  }
?>
