<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {
    function _initialize(){
        import('ORG.Util.Session');
         if(!isset($_SESSION[C('ADMIN_AUTH_KEY')]) || Session::isExpired()){
             $this->error('你尚未登陆', U('Public/login'),$this->isAjax());
         } else {
             Session::setExpire(C('SESSION_EXPIRE_TIME'), true);  //只要有动作就刷新session过期时间
             $admin = Session::get('admin') ;
             $this->assign('admin', $admin);
         }
    }
    public function index(){
    	$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
    	if($page < 1) $page = 1;
    	
    	$status = isset($_GET['status']) ? intval($_GET['status']) : 0;
    	$this->assign('status_code', $status);
    	$this->assign('status', $status == 0 ? '未审核' : ($status == 1 ? '审核通过' : '审核未通过'));
    	
    	$where = ' ischeck=' . $status;
    	if(isset($_GET['keyword'])){
    		$search = trim( $_GET['keyword'] );
    		if(preg_match('/^[\d-]{6,}$/', $search)){
    			$where .= " and `phone`='$search' ";
    		} else {
    			$where .= " and `name` like '%$search%' ";
    		}
    	}
    	
    	$User = D('Users'); 
		$list = $User->relation(true)->where($where)->order('create_time desc')->page($page . ',25')->select();
		$this->assign('list',$list);// 赋值数据集

		import("ORG.Util.Page");// 导入分页类
		$count = $User->where('ischeck=0')->count();// 查询满足要求的总记录数
		$Page  = new Page($count, 25);// 实例化分页类 传入总记录数和每页显示的记录数
		$show  = $Page->show();// 分页显示输出
		$this->assign('page',$show);// 赋值分页输出
		$this->display(); // 输出模板
    }
    
    public function check(){
    	$userid = isset($_POST['userid']) ? intval($_POST['userid']) : 0;
    	$ischeck = isset($_POST['ischeck']) ? intval($_POST['ischeck']) : 0;
    	if(!$userid){
    		$this->ajaxReturn(null, '请选择用户', 0);
    	}
    	if(!$ischeck){
    		$this->ajaxReturn(null, '请输入正确的审核状态', 0);
    	}
    		
    	$user = M('Users');
    	$data['ischeck'] = $ischeck; //审核通过 1 不通过 2
    	$user->where('id=' . $userid)->data($data)->save();
    	
    	$this->ajaxReturn(null, '操作成功', 1);
    }
    
    public function delete(){
        $userid = isset($_POST['userid']) ? intval($_POST['userid']) : 0;
        if(!$userid){
            $this->ajaxReturn(null, '请选择用户', 0);
        }
        $User = D('Users'); 
        if(!($result = $User->relation(true)->where("id=$userid")->find())){
             $this->ajaxReturn(null, '没有该用户', 0);
        }
         //删除文件
        $root_path = getRootPath();
        foreach($result['Files'] as $row){
            if(file_exists($root_path.$row['origin'])){
                unlink($root_path.$row['origin']);
            }
            if(file_exists($root_path.$row['thumb'])){
                unlink($root_path.$row['thumb']);
            }
        }
        //删除记录
        $User->relation(true)->delete("$userid");
        $this->ajaxReturn(null ,'操作成功', 1);
    }
    
    public function setPassword(){
       if($_POST['password']){
           $password = $_POST['password'];
           $password2 = $_POST['password2'];
           if(strlen($password) < 6){
               $this->error('密码不能少于六位');
           }
           if($password != $password2){
               $this->error('两次密码不匹配');
           }
           import('ORG.Util.Session'); 
           $userinfo = Session::get('admin');
           if(!$userinfo){
               $this->error('你尚未登陆', U('Public/login'));
           }
           $Admin = M('Admin');
           $Admin->where('id='.$userinfo['id'])->setField('password',md5(md5($password)));
           unset($_SESSION[C('ADMIN_AUTH_KEY')]);
           unset($_SESSION['admin']);
           $this->success('设置密码成功，请重新登陆', U('Public/login'));
       } else {
           $this->display();
       }
   }
}