<?php
class UsersModel extends RelationModel{
	protected $_link = array(
       'Files'=>  array(  
   	 		'mapping_type'=>HAS_MANY,
            'class_name'=>'Files',
			'parent_key'=>'users_id',
			'mapping_fields'=>'id,origin,thumb,key',
			'mapping_limit'=>'6'
       ),
    );
    
	/**
     * 根据操作步骤获取应上传的文件
     * @param unknown_type $step
     */
    public function getFileInfo($step){
    	switch ($step){
    		case 1:
    			$file_des  = '身份证正面';
		    	$file_key  = 'idcard1';
	    		break;
    		case 2:
    			$file_des  = '身份证反面';
    			$file_key  = 'idcard2';
    			break;
    		case 3:
    			$file_des  = '手持身份证';
    			$file_key  = 'withidcard';
    			break;
    		case 4:
    			$file_des  = '银行卡正面';
    			$file_key  = 'bankcard';
    			break;
    		case 5:
    			$file_des  = '门头照片1';
    			$file_key  = 'door1';
    			break;
    		case 6:
    			$file_des  = '门头照片2';
    			$file_key  = 'door2';
    			break;
    		default:
    			$file_des  = '';
		    	$file_key  = '';
    	}
    	return array('des'=>$file_des, 'key'=>$file_key);
    }
}