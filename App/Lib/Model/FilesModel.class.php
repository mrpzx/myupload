<?php
class FilesModel extends RelationModel{
	  protected $_link = array(
       'Users'=>  array(  
            'mapping_type'=>BELONGS_TO,
            'class_name'=>'Users',
            'foreign_key'=>'users_id',
            'mapping_fields'=>'id,origin,thumb,key'
       )
    );
}