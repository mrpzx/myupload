/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : myupload

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2014-12-08 10:20:32
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `myupload_admin`
-- ----------------------------
DROP TABLE IF EXISTS `myupload_admin`;
CREATE TABLE `myupload_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_login_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myupload_admin
-- ----------------------------
INSERT INTO `myupload_admin` VALUES ('1', 'admin', '14e1b600b1fd579f47433b88e8d85291', '2014-12-08 10:02:51', '1418004171');

-- ----------------------------
-- Table structure for `myupload_files`
-- ----------------------------
DROP TABLE IF EXISTS `myupload_files`;
CREATE TABLE `myupload_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `ext` varchar(20) DEFAULT NULL,
  `key` varchar(20) DEFAULT NULL,
  `hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myupload_files
-- ----------------------------

-- ----------------------------
-- Table structure for `myupload_users`
-- ----------------------------
DROP TABLE IF EXISTS `myupload_users`;
CREATE TABLE `myupload_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ischeck` int(11) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myupload_users
-- ----------------------------
